(function() {
    var readFile = function(file, callback) {
        var reader = new FileReader();
        reader.onload = function(event) {
            callback(event.target.result);
        };
        reader.readAsDataURL(file);
    };

    var debounce = function(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };

    var EDITOR_TEMPLATE = '<input type="file" class="logoEditor-input" accept="image/*">' +
        '<div class="logoEditor-scale" hidden><input type="number" class="logoEditor-scale-input" step="1" min="0" max="500" value="100"></div>' +
        '<a href="" class="logoEditor-download" download="logo.png">Download image</a>';

    var EditorLogo = function(img) {
        this.img = img;
        this.width = img.width;
        this.height = img.height;
        this.x = 0;
        this.y = 0;
        this.scale = 1;
    };

    EditorLogo.prototype.contains = function(x, y) {
        var widthOffset = this.width * this.scale / 2;
        var heightOffset = this.height * this.scale / 2;
        return x >= this.x - widthOffset &&
            x <= this.x + widthOffset &&
            y >= this.y - heightOffset &&
            y <= this.y + heightOffset;
    };

    EditorLogo.prototype.getX = function() {
        return Math.round(this.x - (this.width * this.scale / 2));
    };

    EditorLogo.prototype.getY = function() {
        return Math.round(this.y - (this.height * this.scale / 2));
    };

    var Editor = function(element) {
        this.el = element;
        this.el.innerHTML += EDITOR_TEMPLATE;
        this.input = element.querySelector('input[type="file"]');
        this.download = element.querySelector('a');
        this.canvas = document.createElement('canvas');
        this.ctx = this.canvas.getContext('2d');
        this.scaleInput = element.querySelector('input[type="number"]');

        this.handleBaseRead = this._onBaseRead.bind(this);
        this.handleBaseImgLoad = this._onBaseImgLoad.bind(this);
        this.handleLogoRead = this._onLogoRead.bind(this);
        this.handleLogoLoad = this._onLogoLoad.bind(this);

        this.update = this.update.bind(this);
        this.updateLink = debounce(function() {
            this.download.href = this.canvas.toDataURL();
        }.bind(this), 200);

        this.init();
    };

    Editor.prototype.init = function() {
        this.download.setAttribute('download', this.el.getAttribute('data-export'));
        this._onBaseRead(this.el.getAttribute('data-src'));
        this.input.addEventListener('change', this._onLogoAdd.bind(this));
        this.canvas.addEventListener('mousedown', this._onDragInit.bind(this));
        this.canvas.addEventListener('mousemove', this._onDragMove.bind(this));
        this.canvas.addEventListener('mouseup', this._onDragEnd.bind(this));
        this.canvas.addEventListener('keydown', this._onKeydown.bind(this));
        this.scaleInput.addEventListener('change', this._onScaleChange.bind(this));
        this.scaleInput.addEventListener('keyup', this._onScaleChange.bind(this));
        this.scalar = 1;

        this.canvas.setAttribute('tabindex', '0');
    };

    Editor.prototype._onBaseRead = function(dataUrl) {
        this.baseImg = new Image();
        this.baseImg.onload = this.handleBaseImgLoad;
        this.baseImg.src = dataUrl;
        this.download.href = dataUrl;
        this.input.removeAttribute('hidden');
    };

    Editor.prototype._onBaseImgLoad = function(event) {
        this.canvas.width = this.baseImg.width;
        this.canvas.height = this.baseImg.height;
        this.ctx.drawImage(this.baseImg, 0, 0);
        this.el.appendChild(this.canvas);
    };

    Editor.prototype._onLogoAdd = function(event) {
        if (this.input.files && this.input.files[0]) {
            readFile(this.input.files[0], this.handleLogoRead);
            this.scaleInput.parentNode.removeAttribute('hidden');
        }
    };

    Editor.prototype._onLogoRead = function(dataUrl) {
        this.logoImg = new Image();
        this.logoImg.onload = this.handleLogoLoad;
        this.logoImg.src = dataUrl;
        this.download.href = dataUrl;
    };

    Editor.prototype._onLogoLoad = function(event) {
        this.input.setAttribute('hidden', '');
        this.logo = new EditorLogo(this.logoImg);
        this.logo.x = this.canvas.width / 2;
        this.logo.y = this.canvas.height / 2;
        this.update();
    };

    Editor.prototype.update = function() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.drawImage(this.baseImg, 0, 0);
        this.ctx.drawImage(
            this.logo.img,
            this.logo.getX(),
            this.logo.getY(),
            this.logo.width * this.logo.scale,
            this.logo.height * this.logo.scale
        );
        this.updateLink();
        if (this.dragging) {
            window.requestAnimationFrame(this.update);
        }
    };

    Editor.prototype._onDragInit = function(event) {
        this.scalar = this.el.offsetWidth / this.canvas.width;
        var actualX = event.offsetX / this.scalar;
        var actualY = event.offsetY / this.scalar;
        if (!this.logo || !this.logo.contains(actualX, actualY)) {
            return;
        }
        this.dragX = actualX;
        this.dragY = actualY;
        this.dragging = true;
        window.requestAnimationFrame(this.update);
    };

    Editor.prototype._onDragMove = function(event) {
        var actualX = event.offsetX / this.scalar;
        var actualY = event.offsetY / this.scalar;
        event.preventDefault();
        if (!this.dragging) {
            return;
        }
        this.logo.x += actualX - this.dragX;
        this.logo.y += actualY - this.dragY;
        this.dragX = actualX;
        this.dragY = actualY;
    };

    Editor.prototype._onDragEnd = function(event) {
        event.preventDefault();
        this.dragging = false;
    };

    Editor.prototype._onKeydown = function(event) {
        var keyCode = event.keyCode || event.which;
        if (keyCode < 37 || keyCode > 40) {
            return;
        }

        // handle arrow keys
        event.preventDefault();
        switch (keyCode) {
        case 37:
            this.logo.x -= 1;
            break;
        case 38:
            this.logo.y -= 1;
            break;
        case 39:
            this.logo.x += 1;
            break;
        case 40:
            this.logo.y += 1;
            break;
        }

        this.update();
    };

    Editor.prototype._onScaleChange = function(event) {
        this.logo.scale = this.scaleInput.value / 100;
        this.update();
    };

    var editors = document.querySelectorAll('.js-logo');
    Array.prototype.forEach.call(editors, function(el) {
        return new Editor(el);
    });
}());
